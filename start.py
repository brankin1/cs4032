#!/usr/bin/env python

#Name: James Brankin
#Student No: 09050761

from sys import argv
import concurrent.futures
import socketserver
import fcntl
import struct
import socket
import pdb

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    def process_message(self):
        if (self.data.startswith(b'HELO')):
            print(self.data.decode("utf-8")+"IP:" + server.server_address[0]+"\nPort:"+ str(server.server_address[1]) +"\nStudentID:09050761")           
            self.request.sendall(bytes(self.data.decode('utf-8') +"IP:" + server.server_address[0]+"\nPort:"+ str(server.server_address[1]) +"\nStudentID:09050761", "utf-8"))
        elif (self.data == b'KILL_SERVICE\n'):
            print("\"KILL_SERVICE\\n\" received. Forcing shut down on the server")
            server.server_close()
        else:
            print("The messages you process would go here")
            self.request.sendall("")

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024)
        print("{} wrote:".format(self.client_address[0]))
        print(self.data)

        #Using the concurrent.futures package, we can create a threadpool and hand each client connection off to the threadpool
        with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
            executor.submit(self.process_message)

HOST = "127.0.0.1"
PORT = 9000

                    
#This checks to see if the amount of arguments is correct
if len(argv) > 3:
    raise Exception("Too many arguments. Try with just a port number or no arguments. Default server address is 127.0.0.1:9000")
#Do some error checking on the port before we bind to it
if len(argv) == 2:
    try:
        PORT = int(argv[1])
    except ValueError:
        raise Exception("Port isn't a number") 
if len(argv) == 3:
    try:
        PORT = int(argv[1])
        HOST = socket.gethostbyname(socket.gethostname())
    except ValueError:
        raise Exception("Port isn't a number")
        
if __name__ == "__main__":
    # Create the server, binding to localhost on port
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)
    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    print("Server binded to " + HOST + ":" + str(PORT))
    server.serve_forever()
